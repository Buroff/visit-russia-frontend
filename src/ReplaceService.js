import Vue from 'vue';
import _ from 'lodash';
import VueResource from 'vue-resource'; 
Vue.use(VueResource);
import EventService from './EventService.js';

// base URL
// Vue.http.options.root = 'http://visitrussia.dev.ad-rus.com/api/';

export default new Vue({ 
	name: 'ReplaceService',  
	data(){ // данные из ProductList
		
		return {		
		    // --- API ---
		    sourceHotels:'read/hotels/',	
		    sourceDeleteHotel: 'orders/delete/hotels/',
		    sourceDeleteTransport: 'orders/delete/transports/', // удаляем транспорт
		    sourceDeletePlaces: 'orders/test/deleteplaces/',  

		    sourceAddTransport:'orders/write/transports/', // добавляем транспорт 
		    sourceUpdateTransport:'orders/update/transports/', // обновляем транспорт	
		    sourceHotelAdd: 'orders/write/hotels/', // добавление отеля
		    sourceHotelUpdate: 'orders/update/hotels/',
		    sourceAddPlaces: 'orders/test/addplaces/',

		    sourceCityOrderid: 'orders/ordercityid/',  // получаем id города из order_cities
		    sourceUpdateCityDate: 'orders/update/cities/', // обновляем город
		    sourceUpdateDateCity:'orders/test/updatedatecity/',

		    sourceAddCity: 'orders/write/cities/', // добавляем промежуточный город
		    sourceAddCityToBasket:'orders/test/writecity/', 	 

		    // --- переменные хранилища
		    timeInterval:false, // интервал времени, который будет добавляться ко всем элементам nextcityInfo.plan		    
		    beginMiddle:false,
		    endMiddle:false,
		    toMiddleTransport:false,

		    // город куда мы возврщаемся
            final_city:false,

            // данные текущего заказа
            order:false,
		    cityInfo:[], // информация о начальном городе городе            
            cityID:false,
            deletedCityPlaces:[],
            addCityPlaces:[],
            cityOrder:false, // информация о начальном городе из таблицы orders_cities  

            // конечный город
            nextcityInfo:[], 
            nextcityID:false,            
            nextCityPlaces:[], // места конечного города в хранилище            
            nextCityOrder:false, // информация о конечном городе из таблицы orders_cities 
            addedPlaces:[], // добпавленные в корзину места

            // промежуточный город
            middleCityID:false,
		    middleCityDates:false,
		    type:false,
		    middle:false,
		    timeInterval:false,

            // состояния
            updateState:'init',
            addState:'init'
        }	    
	},

	methods:{ 
		

		resetStorage: function(){
			this.timeInterval = false;
		    this.toMiddleTransport = false;

		    this.cityInfo.length = 0; // информация о начальном городе городе            
            this.cityID = false;
            this.deletedCityPlaces.length = 0;
            this.addCityPlaces.length = 0;
            this.cityOrder = false; // информация о начальном городе из таблицы orders_cities 

            this.nextcityInfo.length = 0;
            this.nextcityID = false;            
            this.nextCityPlaces.length = 0; // места конечного города в хранилище            
            this.nextCityOrder = false;

            this.middleCityID = false;
		    this.middleCityDates = false;
		    this.type = false;
		    this.middle = false;
		    this.timeInterval = false;

		    this.updateState ='init';
            this.addState ='init';

		},

		// resetDates: function(){
		// 	this.timeInterval = false;
		//     this.beginMiddle = false;
		//     this.endMiddle = false;
		// },

		// запись в хранилище данных о начальном и конечном пунктах 
		// перед выбором промежуточного
		initStorage:function (Params){
			console.log('--- initStorage ---');		

			console.log(Params);
			console.log(Params.city_info.city_plan);
			return;

			if(this.cityInfo.length>0){
				this.resetStorage();
			}

			this.cityInfo = Params.city_info;
            this.cityID = Params.city_id;
            this.nextcityInfo =  Params.next_city_info;
            this.nextcityID = Params.next_city_info.city_info.id;  

            //   

            // сохраняем в хранилище места (отели/транспорт)
            this.setReplacedNextCityData(); 
            this.setDeletedCityData();
   			this.PlaceMiddleCity(Params.middle_city_transport.middle_city_id, Params.type, Params.middle);
		},
		
		//  PlaceMiddleCity: function(middleCityID, type = false, middle = false){
		PlaceMiddleCity: function(middleCityID, type = 'create', middle = false){
	      		console.log('--- placeMiddleCity ---');
	      		// return;

	      		// даты выезда из начального города
		        let beginCityPlan = this.cityInfo.city_plan;
		        let beginCityEndDate = beginCityPlan[beginCityPlan.length-1].end;

		        // даты и интервал для ПРОМЕЖУТОЧНГО города
		        let middleCityBeginDate = (3600 * 12)+beginCityEndDate;
		        let middleCityEndDate = ((3600 * 24)*1)+beginCityEndDate;
		        let middleCityDates = {
		              date_from:middleCityBeginDate,
		              date_to:middleCityEndDate
		        }

	            // даты КОНЕЧНОГО ГОРОДА
	            let nextCityPlan = this.nextcityInfo.city_plan;
	            let nextCityBeginDate = nextCityPlan[0].begin;
	            let nextCityEndDate = nextCityPlan[nextCityPlan.length-1].end;

	      		// интервал сдвига дат для КОНЕЧНОГО ГОРОДА
	            let timeInterval =  ((3600 * 24)*1); // 1 день промежуточного 
	            

	            // новые значения дат КОНЕЧНОГО города
	            let nextCityDates = {
	                date_from:nextCityBeginDate + timeInterval,
	                date_to:nextCityEndDate + timeInterval
	            } 

	            // console.log('-- middleCityDates --');
	            // console.log(middleCityDates);
	            // console.log('-- middleCityID --');
	            // console.log(middleCityID);
	            // console.log('-- type --');
	            // console.log(type);
	            // console.log('-- middle --');
	            // console.log(middle);
	            // console.log('-- timeInterval --');
	            // console.log(timeInterval);
	            // console.log('-- deletedCityPlaces --');
	            // console.log(this.deletedCityPlaces);
	            // return false;

	            
	            if(this.deletedCityPlaces.length>0){ 
		             // удаляем  начальному городу транспорт в конечный  
		             this._deletedPlaces('departing');
		        }

		        // сохраняем в data
		   		this.middleCityID = middleCityID;
		        this.middleCityDates = middleCityDates;
		        this.type = type;
		        this.middle = middle;
		        this.timeInterval = timeInterval;


	            // обновляем дату у города
		        this.UpdateDateCity(this.nextcityInfo.city_info.id, 'arrival', timeInterval);

		        
		       	/*if(this.nextCityPlaces.length>0){
			        // удаляем места у конечного города
			        this._deletedPlaces('arrival', this.nextCityPlaces);		              
			            
			        // добавляем конечному городу из хранилица сохраненные отели (со сдвигом даты)
			        this.getDataFromStoroge('arrival', this.nextCityPlaces, timeInterval);
			    } 		*/ 

			    // добавляем промежуточный город
	 			this.AddCityToBasket(middleCityID, middleCityDates, type, middle, timeInterval);

	    },

		// updateCityDate: function (cityID = 12, type = 'arrival')
		UpdateDateCity: function (cityID, type = 'arrival', time_interval = false){
			console.log('---UpdateCityDate ---'); 
			
			let options = { 
				token: 'admin_token', 
				id_city:cityID
			}
        	
        	if(type == 'arrival'){
          		options.time_interval = time_interval;
        	}

        	// console.log(options);        	
        	// console.log(this.sourceUpdateDateCity);
        	// return;

        	this.$http.post(this.sourceUpdateDateCity, options).then(function (response) {
        		console.log(response.body);
        		this.updateState = 'sent';
        	});        

		},

	    AddCityToBasket: function (cityID, cityDates, type = false, middle = false, time_interval = false){		
			console.log('--- AddCityToBasket  ReplaceService---'); 
			// console.log(cityID);
			// console.log(cityDates);  

	        let options = { 
	              token:'admin_token', 
	              id_city:cityID,
	              type: type
	        }

	        if(cityDates){
	        	options.date_from = cityDates.date_from;
	        	options.date_to = cityDates.date_to;

	        }

	        if(time_interval){
	        	options.time_interval = time_interval;
		        
		        if(middle){
		        	options.middle = middle;
		        }
	        }       


	        // console.log(options);
	        // console.log(this.sourceAddCityToBasket);
	        // // this.addState = 'test';
	        // return;
	           
	        this.$http.post(this.sourceAddCityToBasket, options).then(function (response) {	 
	        		// setTimeout(EventService.updatePlanningList(true), 15000);  
	        		this.addState = 'sent';

	      	});
		},

		_deletedPlaces: function (type, nextCityPlaces = false, addPlace = false, transport = false, ) {          
		        console.log('--- test deletedPlaces ---'); 
		        let options = {
		          token:'admin_token',
		          places:[]
		        }
		        
		        if(type == 'arrival' && nextCityPlaces.length>0){
		            options.places = nextCityPlaces;
		        }

		        if(type == 'departing' && this.deletedCityPlaces.length>0){
		          let places = [];
		            for(let i = 0; i<this.deletedCityPlaces.length; i++){       
		              
		              if(this.deletedCityPlaces[i].type == 'transport'){
		                places.push(this.deletedCityPlaces[i]);
		              }
		            }
		            options.places = places;
		        }

		        console.log(options);

		        // отправляем запрос на удаление массива мест на сервер
		        if(options.places.length >0){
		            this.$http.post(this.sourceDeletePlaces, options).then(function (response) { 
		                console.log(response.body);
		                if(typeof addedPlace == 'object'){
		                  console.log('--- все удалено ---');
		                }
		            });
		        }
		},	

	    getDataFromStoroge: function(type, nextCityPlaces, timeInterval = 0){
			 console.log('--- getDataFromStoroge ---'); 
			
			let transports = [];
		    let places = [];
		    let options = {
		        token:'admin_token',        
		    }

		    if(type == 'arrival'){ 
			    if(nextCityPlaces.length>0){
			    	for (let i = 0; i<nextCityPlaces.length; i++){
			    	
			            if(nextCityPlaces[i].type != 'transport'){
			            	nextCityPlaces[i].date_from = nextCityPlaces[i].date_from + timeInterval;
	                        nextCityPlaces[i].date_to = nextCityPlaces[i].date_to + timeInterval;
	                        places.push(nextCityPlaces[i]);
			            }
			    	}
			    }
			}

		    if(places.length>0){ 
        		options.places = places;
        	}

        	console.log(JSON.stringify(options.places)); 
      		// console.log(options.places);


      		
            this.$http.post(this.sourceAddPlaces, options).then(function (response) {
		        console.log(response.body);
		      // setTimeout(EventService.updatePlanningList(true), 15000);
		    });
		},	

		


		// массив транспорта/отелей для города назаначения
	    setReplacedNextCityData:function(){
		    console.log('--- setReplacedNextCityData ---');

		    if(this.nextcityInfo.city_plan.length>0){

		    	// console.log(this.nextcityInfo);

		      	let places = [];		      	
		        
		        let transports = [];
		        let hotels = [];
		        let hotelsUnique = []; // список уникальных записей
		        let clinics = [];
		        let clinicsUnique = [];
		        let tours = [];
		        let toursUnique = [];
		        let restaurants = [];
		        let restaurantsUnique = [];

		        // console.log(this.nextcityInfo.city_plan);


		        for (let i = 0; i<this.nextcityInfo.city_plan.length; i++) {

		        	// console.log('--- nextcityInfo.city_plan[i].place ---');
		        	
		        	if(typeof this.nextcityInfo.city_plan[i].place !== "undefined" && this.nextcityInfo.city_plan[i].place.length > 0) {

		        		this.nextcityInfo.city_plan[i].place.forEach(function(place, index){  

				          	if(place.type == 'transport'){
				                // transports.push(place);
				            }            
				              
				            if(place.type == 'hotel'){
				            	hotelsUnique[place.id] = place;}

				            if(place.type == 'clinic'){	             	
				                clinicsUnique[place.id] = place;}

				            if(place.type == 'tour'){	             	
				                toursUnique[place.id] = place;}

				            if(place.type == 'restaurant'){	             	
				                restaurantsUnique[place.id] = place;}
			             
			            });   
					}

		        }

		        // сохраняем уникальные значения в список отелей
		        hotelsUnique.forEach(function(place, index){ 
		        	hotels.push(place);
		        });

		        clinicsUnique.forEach(function(place, index){ 
		        	clinics.push(place);
		        });

		        toursUnique.forEach(function(place, index){ 
		        	tours.push(place);
		        });

		        restaurantsUnique.forEach(function(place, index){ 
		        	restaurants.push(place);
		        });

		        // console.log(transports);
		        // console.log(hotels);

		        this.nextCityPlaces = places.concat(transports, hotels, clinics, tours, restaurants);
		        
		        // console.log(this.nextCityPlaces);
		        // console.log(JSON.stringify(this.nextCityPlaces));        
		    }
		    // this.getReplaced();
	    },

	    // массив удаленного транспорта для города отправления
	    setDeletedCityData: function(){
	      // console.log('--- DeletedCityData ---');
	      
	      if(this.cityInfo.city_plan.length>0){
	        let transports = [];

	        // проверяем последний день начального города
	        let length = this.cityInfo.city_plan.length;
	        this.cityInfo.city_plan[length-1].place.forEach(function(place, index){
	          if(place.type == 'transport'){
	            transports.push(place);
	          }
	        });

	        if(transports.length>0){
	        	this.deletedCityPlaces = transports;
	        }
	        
	      }
	    }

	    
	},

	created: function() {	

	},

	updated: function(){
		// this.showHotels = !this.showHotels;
	},

	watch:{
			addedPlaces:function(value){		

				if(value.length>0){
					console.log('--- watch addedPlaces ---');
					console.log(value);
					// console.log('--- nextCityPlaces ---');
					// console.log(this.nextCityPlaces);

					if(value.length == this.nextCityPlaces.length){	

						// обновляем список Planning List
			            // установить событие для Planning getCityList    
			            // EventService.updatePlanningList(true);
			            // setTimeout(EventService.updatePlanningList(true), 15000);
			            EventService.updatePlanningList(true);
					}

				}

				// ---/---
			},

			updateState:function(value){

				if(value == 'sent'){

				    console.log('--- watch updateState ---');
				    console.log(this.updateState);

				    if(this.nextCityPlaces.length>0){
				        // удаляем места у конечного города
				        this._deletedPlaces('arrival', this.nextCityPlaces);		              
				        // добавляем конечному городу из хранилица сохраненные отели (со сдвигом даты)
				        this.getDataFromStoroge('arrival', this.nextCityPlaces,this.timeInterval);
			    	} 	

			    	// добавляем промежуточный город
		 			this.AddCityToBasket(this.middleCityID, this.middleCityDates, this.type, this.middle, this.timeInterval);

				}

			},

			addState:function(value){
				if(value == 'sent'){
					console.log('--- updatePlanningList ---');
					setTimeout(EventService.updatePlanningList(true), 15000);  
				}else{
					console.log('--- watch addState ---');

				}
			}

	}
});
