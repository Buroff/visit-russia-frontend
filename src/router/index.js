import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Forgotpass from '@/components/Forgotpass'
import PlanningRoute from '@/components/PlanningRoute'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/forgotpass/:code',
      name: 'Forgotpass',
      component: Forgotpass
    },
    {
      path: '/planningroute',
      name: 'PlanningRoute',
      component: PlanningRoute
    }
  ]
})
