// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import App from './App';
import router from './router';
var VueCookie = require('vue-cookie');
Vue.config.productionTip = false;

import VueResource from 'vue-resource';

Vue.use(VueCookie);
Vue.use(VueResource);
// Vue.http.options.root = 'http://visitrussia.dev.ad-rus.com/api/';
Vue.http.options.root = 'http://visitrussia-backend/api/';
// Vue.http.options.root = 'http://vr_backend.dev.local/api/';

/* eslint-disable no-new */
new Vue({
  el: '#app',
  created: function (){
    // VueCookie.delete('data_user')
    if(!VueCookie.get('data_user')){

        let randomstring = Math.random().toString(36).slice(-10)+"@"+Math.random().toString(36).slice(-10)+".ru";

        this.$root.dataLoad = true;
        Vue.http.post('account/guest/', {email: randomstring}).then(response => {
          
            this.$root.dataLoad = false;
            this.$root.sign = true;
            this.$root.dataUser = JSON.parse(response.bodyText);
           //this.someData = response.body;

           VueCookie.set('data_user', response.bodyText);

         }, response => {
         });
    }else{
      this.$root.dataUser = JSON.parse(VueCookie.get('data_user'));
      if(this.$root.dataUser.order){
        this.$router.push('/planningroute');
      }
    }
  },
  data (){
    return {
      dataLoad: false,
      dataUser: {},
      sign: true
    }
  },
  router,
  template: '<App />',
  components: { App }
})
