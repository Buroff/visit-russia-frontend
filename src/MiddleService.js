import Vue from 'vue'
import _ from 'lodash';
import VueResource from 'vue-resource'; 
Vue.use(VueResource);


import EventService from './EventService.js';
import ReplaceService from './ReplaceService.js';
import MiddleService from './MiddleService.js';
import ReplaceFinalService from './ReplaceFinalService.js';

// base URL
// Vue.http.options.root = 'http://vr_backend.dev.local/api/';
export default new Vue({  

	data(){ // данные из ProductList
		return {

			// API
			sourceMiddleTransport:'read/searchmiddlecity/',
            sourceDeleteMiddles:'orders/test/deletemiddles/',
			//           
            message: {
                  title:'Нет городов',
                  show:false,
                  warning:false
            },
            show:false,
            move:false, // пожно ли сдвинуть даты у города конечного назмачения

		}	    
	},
	methods:{ 
		
		// ищем промежутьочные города
        setMiddles:function (cities) {
            console.log('--- setMiddles ---');

            for (let i = 0; i < cities.length; i++){
                // console.log(cities[i]);
                this.paramMiddleCity(cities[i].city_info.id, cities[i], i, cities);
            }
        },

        deleteMiddles:function (cities){
            console.log('--- deleteMiddles ---');           

            let middles = [];                
            for (let i = 0; i < cities.length; i++){                

                if(cities[i].middle){
                    middles.push(cities[i]);
                    cities.splice(i,1);                        
                }   
            }

            //  console.log(cities);
            //  console.log(middles);

            let oplions = {
                token:'admin_token',                
                cities:cities
            }


            if(middles.length > 0){
                oplions.city_middles = middles;
            }

            console.log(oplions);   
            // let jsonParams = JSON.stringify(oplions);    
           console.log(this.sourceDeleteMiddles);
            // console.log(jsonParams);     

            // --- пишем код для удаления       
            this.$http.post('orders/test/deletemiddles', oplions).then(function (response) {     
                    // setTimeout(EventService.updatePlanningList(true), 15000);  
                    // ставим событие в MiddleService       
                    console.log(response.body);
            });  
        },


        // параметры промежуточного города
        paramMiddleCity:  function(cityID, cityInfo, index, cities){
            // console.log('--- paramMiddleCity ---');
            // console.log(cityID);
            // console.log(cityInfo);
            // console.log(index);
            // console.log(cities);
            // console.log('--- / ---');
            // return;

         
            let nextcityInfo = [];        
            let transportsFrom = [];
            let transportsTo = [];     
            let orderCities = [];     
            let nextcityID = false;
            let nextCityInfo = false;
            let numbers = false;

            if(typeof cities[index+1] == 'object'){
                nextCityInfo = cities[index+1];  

            }
            numbers = cities.length;            

            // console.log(index);
            // console.log(numbers);
            // console.log(cities);
            // console.log(nextCityInfo);
            // console.log(typeof(cities[index+1]));
            // console.log('--- / ---');
            // return;
            
            
            // if((numbers - (index+1))<=1 && nextCityInfo.length >0){
            if(nextCityInfo){
                nextcityID = nextCityInfo.city_info.id;
                orderCities = nextCityInfo;

                // --- find middle transport
                let options = {
                    сity_from: cityID,
                    сity_to: nextcityID
                }
                this.findMiddleTransport(options, cityID, cityInfo, nextCityInfo, orderCities);  

                // console.log('--- init findMiddleTransport ---');            
            }

        },

        // поиск промежуточных городов
        findMiddleTransport: function (options, cityID, city, nextCity, orderCities){

        	console.log('--- findMiddleTransport ---');
            // console.log(cityID);
            // console.log(options);
            // console.log(nextCity);
            // console.log(orderCities);

            // // let jsonOptions = JSON.stringify(options);
            // // console.log(jsonOptions);
            // console.log('--- / ---');
            // return;

        	// console.log(nextCity);

        	let middleCities = [];
            let transportsFrom = [];
            let transportsTo = [];

            // console.log(options);

            this.$http.post(this.sourceMiddleTransport, options).then(function (response) {
                // console.log(response.body);
                middleCities = response.body.cities;
                transportsFrom = response.body.transports_from;
                transportsTo = response.body.transports_to;  

                // console.log(middleCities);
                // console.log(transportsFrom);
                // console.log(transportsTo);

                // !!! promise             
 
                if(middleCities.length>0){
	                	for(let i = 0; i < middleCities.length; i++){
	                		this._addCityToBasket(middleCities[i], cityID, city, nextCity, transportsFrom, transportsTo, orderCities, middleCities);
	                	}
                }

            });
       },        

        // добавляем в корзину 
        _addCityToBasket:function(middleCity, cityID, city, nextCity, transportsFrom, transportsTo, orderCities, middleCities){

            console.log('--- _addCityToBasket ---');

            let transport = this.getMiddleCityTransport(middleCity.id, transportsFrom, transportsTo);
            let Params = {
                  city_info:city,
                  next_city_info:nextCity,
                  city_id:cityID,
                  middle_city_transport:transport,
                  type:'create',
                  middle:1                  
            }

            
            let jsonParams = JSON.stringify(Params);
            let jsonTransport = JSON.stringify(transport);
            // console.log(jsonParams);
            // console.log(jsonTransport);

            console.log(Params);
            console.log(transport);

            return;
       

            if(nextCity.final == 1){ // когда добавляем  промежуточный город перед финальным
                    /*console.log('--- Basket final ---');
                    // console.log(city.city_info.title);                  
                    ReplaceFinalService.initStorage(Params);*/
            }
            else{ // когда добавляем промежуточный город перед обычным
                    console.log('--- Basket not ---');
                    console.log(city.city_info.title);
                    ReplaceService.initStorage(Params); 
            }
            

            // if(nextCity.final == 1){        
                //     // --- меняем и добавляем в MiddleService !!!
    	           //      ReplaceFinalService.initStorage(Params);
    	           //      // setTimeout(this.closeWindow(), 10000);
                      
                // }else{         
                                
                //     console.log(Params);

                //     //проверка на то есть ли промежуточный город в текущем заказе
                //     // проверка на то есть ли промежуточный город в текущем заказе
                //     let orderMiddleCity = this.checkOrder(middleCities, orderCities);

    	           //  if(orderMiddleCity){
    	           //      // this.message.show = true;
    	           //      // this.message.title = 'Добавить промежуточный пункт нельзя для данного города.'
    	           //      // this.message.warning = 'Так как один из промежуточных пунктов уже есть в заказе.';                     
    	           //  }
    	           //  else{
    	                
                //         // --- меняем и добавляем в MiddleService !!!
    		          //   ReplaceService.initStorage(Params); 
    		          //   // setTimeout(this.closeWindow(), 10000);
    	           //  }


            // }


        },

        // получить транспорт для промежуточного города
        getMiddleCityTransport: function(middleCityID, transportsFrom, transportsTo){

              // console.log('--- getMiddleCityTransport ---');
              
	            let from = transportsFrom.filter(function(values, item){
	                      if(values.id_city_to == middleCityID){
	                        return values;
	                      }
	            });

	            let to = transportsTo.filter(function(values, item){
	                      if(values.id_city_from == middleCityID){
	                       return values;
	                      }
	            });

	            let transport = { 
	                transports_from:from, 
	                transports_to:to, 
	                middle_city_id:middleCityID 
	            }

                return transport;
        },

        // проаеряем заказ
        checkOrder: function(middleCities, orderCities){
            console.log('--- checkOrder ---');
              
            let middleCity = false;
            let middles = [];
              // let orders = [];

            middleCities.filter(function(values, item){                     
                if(values.id){
                    middles.push(values.id);
                }
            });

            top:
            for (let i = 0; i < middles.length; i++){
	                for (let j = 0; j < orderCities.length; j++) {                     
	                    
	                    if(orderCities[j].city_info.id ==  middles[i]){
	                        middleCity = orderCities[j].city_info;
	                        middleCity.final = orderCities[j].final
	                        break top;
	                    }
	                }
            }
            // console.log(middleCity);   
            return middleCity;         
              
        }

	},
	created: function() {	

	},
	updated: function(){
		// this.showHotels = !this.showHotels;
	},
	watch:{

	}
});
