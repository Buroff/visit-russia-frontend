import Vue from 'vue'
import _ from 'lodash';
import VueResource from 'vue-resource'; 
Vue.use(VueResource);
// base URL
// Vue.http.options.root = 'http://vr_backend.dev.local/api/';
export default new Vue({  

	data(){ // данные из ProductList
		return {
			// тестовые данные
		    products:[ 
		    	{
	                id: 1,
	                title: 'Product #1',
	                price: 50,
	                qt: 1000
	            },
	            {
	                id: 2,
	                title: 'Product #2',
	                price: 100,
	                qt: 456
	            }
		    ],

		    // --- API
		    sourceHotels:'read/hotels/',
		    sourceHotelData: 'read/hotels/',
		    
		    // --- Planing
		    cities:[], // список городов 
		    cityid:false,
		    // --- HotelAdd
		    showHotels: false,
		    currentCity:{}, // информация по текущему городу
		    // --- HotelAdd
		    showHotels: false,
		    currentCity:{},
		    // --- HotelDetail
		    showHotelDetail:false, 

		    // --- число взрослых и детей в туре
		    travelers:false

		}	    
	},
	methods:{ // метод из ProductList
		// устанавливаем список городов текущего пользователя
		setCities:function(citiesParam){
			this.cities = citiesParam;
			// this.checkPlaces(); // !!! EventService.checkPlaces()
		},

		// получаем информацию по текущему городу
		getCityData: function(cityID){
			// let currentCity = _.find(this.cities, {sity_info.id:cityID});
			let city = false;
			_.find(this.cities, function(value, key){
				if(value['city_info']['id'] == cityID){
					city = value['city_info'];
				}
			});
			// this.currentCityInfo = city;
			return city;
		},

		// --- HotelAdd
		// получаем информацию по списку отелей в текущем городе
		cityHotels:function(cityID){
				let hotels = [] // массив отелей по городу

				// формируем запрос к API для получения списка отелей
                let options = {
                        token: 'admin_token',
                        cities: cityID
                }                  

                this.$http.post(this.sourceHotels, options).then(function (response) {                     
                      for(var i = 0; i<response.data.length; i++){
                          //console.log(response.data[i])
                          hotels.push(response.data[i]); // сохраняем в переменную
                      }
                  },function (error) {
                      console.log(error);
                  });

                return hotels;
		},
	    
	    // список отелей
	    viewHotels:function(sityID, cityPlan){	
	    	let hotelListToView = this.cityHotels(sityID);
	    	this.currentCity.cityInfo = this.getCityData(sityID);
	    	this.showHotels = true;

	    	let hotelData = {
	    		hotels:hotelListToView,
	    		cityinfo:this.currentCity.cityInfo,
	    		cityplan:cityPlan,
	    		show:this.showHotels
	    	}  	 

	    	// console.log(this.currentCity);
	    	this.$emit("viewHotels", hotelData); // создаем событие
	    },

	    closeHotelsWindow: function(){
	    	this.$emit("closeHotelsWindow"); // создаем событие
	    },

		// --- HotelDetail
		// окно детального описания отеля
		// viewHotelDetail:function(hotelID, currentCity, cityPlan){ 	
		//    	let hotelData = {
		//    		hotelID:hotelID,
		//    		cityinfo:currentCity,
		//    		cityplan:cityPlan
		//    	}
		//    	// console.log('--- EventService ---');
		//    	console.log(hotelData);
		//    	this.$emit("viewHotelDetail", hotelData); // создаем событие
		// },

		viewHotelDetail:function(Param){ 
	    	this.$emit("viewHotelDetail", Param); // создаем событие
	    },




	    // --- AddTransport
	    viewTransport: function(cityID, city, nextcity){
	    	let Param = {
	    		city:city,
	    		cityId:cityID,
	    		nextCity:nextcity

	    	}
	    	console.log('--- emit ---');	    	
	    	// console.log(Param);	    	
	    	// создаем событие
	    	this.$emit("viewTransportList", Param);
	    },

	    // --- AddMiddlecity
	    viewMiddlecity: function(Params){
	    	// console.log('--- emit ---');    
	    	this.$emit("viewMiddlecityList", Params);	     
	    }, 

	    viewStaticMiddlecity: function(Params){
	    	// console.log('--- emit ---');	     
	    	this.$emit("viewStaticMiddlecityList", Params);	     
	    }, 

	    // --- updatePlanningList
	    updatePlanningList: function(update){
	    	console.log('--- updatePlanningList ---');
	    	this.$emit("planningListUpdate", update);
	    },

	    // --- viewDeletePlace
	    viewDeletePlace: function (Params){
	    	this.$emit("viewPlaceDelete", Params);
	    },
	    
	    // --- viewAddDay
	    viewAddDay: function (Params){
	    	this.$emit("viewDay", Params);
	    },
		
		// ---  addTransport
	    addTransportFrom: function(transport){
	    	this.$emit("addTransportFrom", transport);
	    	// console.log('addTransportFrom');
	    },

	    // --- viewCalendar
	    viewCalendar: function(cityID){
	    	// console.log('--- viewCalendar ---')
	    	this.$emit("viewCalendar", cityID);
	    },

	    // --- Traveling
	    traveling: function(adults, children){
	    	let Param = {
	    		num_adults:adults,
	    		num_children:children
	    	}
	    	this.$emit("traveling", Param);
	    },

	    tourists: function (adults, children){
	    	let Param = {
	    		adults:adults,
	    		children:children
	    	}

	    	// this.$emit("traveller", Param);
	    	this.travelers = Param;
	    },

	    // --- viewClinics
	    viewClinics:function(Param){  
	    	this.$emit("viewClinics", Param); // создаем событие
	    },

		viewClinicDetail:function(Param){ 
	    	this.$emit("viewClinicDetail", Param); 
	    },

	    closeClinicsWindow: function(){
	    	this.$emit("closeClinicsWindow"); 
	    },

	    // --- viewTours
	    viewTours:function(Param){  
	    	this.$emit("viewTours", Param); 
	    },

	    viewTourDetail:function(Param){ 
	    	this.$emit("viewTourDetail", Param); 
	    },

	    closeToursWindow: function(){
	    	this.$emit("closeToursWindow"); 
	    },

	    // --- viewRestaurants
	    viewRestaurants:function(Param){  
	    	this.$emit("viewRestaurants", Param);
	    },

	    viewRestaurantDetail:function(Param){ 
	    	console.log('emit');
	    	this.$emit("viewRestaurantDetail", Param); 
	    },

	    closeRestaurantsWindow: function(){
	    	this.$emit("closeRestaurantsWindow"); 
	    },

	    // --- DeleteCityTest
	    viewDeleteCityTest: function(Params){
	    	// console.log('--- viewDeleteCityTest ---')
	    	this.$emit("viewDeleteCityTest", Params);	 
	    },

	    // если выбран финальный город передаем даные в Planing
	    // finalCity: function(Params){
	    // 	console.log('--- finalCity ---');
	    // 	this.$emit("finalCitySelected", Params);
	    // }

	    //  обновляем проверку мест у городов
	    checkPlaces: function(){
	    	// console.log('-- checkPlaces --');
	    	let options = {
              	token: 'admin_token'
            } 
           this.$http.post('orders/read', options).then(function (response) {                    
                    // console.log(response.body.cities); 
                    this.$emit("checkPlacesUpdate", response.body.cities);	
            }); 
	    },

	    getOrderData:function (){
              console.log('-- getOrderData --');
              let options = {
              	token: 'admin_token'
              } 
              this.$http.post('orders/read', options).then(function (response) {                    
                     console.log(response.body); 
              });
        },
	    
	},
	created: function() {	

	},
	updated: function(){
		// this.showHotels = !this.showHotels;
	}
});
